import random
import string

import requests
from lxml import etree


def generate_random_id_for_agent():
    characters = string.ascii_letters + string.digits + '-_'
    ran_id = ''.join(random.choice(characters) for _ in range(21))
    return ran_id


def get_bank_of_canada_exchange_rates():
    boc_url = "https://www.bankofcanada.ca/valet/fx_rss"

    try:
        response = requests.get(boc_url, timeout=10)
        response.raise_for_status()
        xml_content = response.content

        root = etree.fromstring(xml_content)

        namespaces = {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "cb": "http://www.cbwiki.net/wiki/index.php/Specification_1.1",
            "dc": "http://purl.org/dc/elements/1.1/",
            "default": "http://purl.org/rss/1.0/",
        }

        exchange_rates = []

        for item in root.xpath("//default:item", namespaces=namespaces):
            try:
                title = item.find("default:title", namespaces).text.strip() if item.find("default:title",
                                                                                         namespaces) is not None else None
                description = item.find("default:description", namespaces).text.strip() if item.find(
                    "default:description", namespaces) is not None else None
                link = item.find("default:link", namespaces).text.strip() if item.find("default:link",
                                                                                       namespaces) is not None else None
                date = item.find("dc:date", namespaces).text.strip() if item.find("dc:date",
                                                                                  namespaces) is not None else None

                stats = item.find("cb:statistics", namespaces)
                exchange_rate = stats.find("cb:exchangeRate", namespaces) if stats is not None else None

                value = float(exchange_rate.find("cb:value",
                                                 namespaces).text.strip()) if exchange_rate is not None and exchange_rate.find(
                    "cb:value", namespaces) is not None else None
                base_currency = exchange_rate.find("cb:baseCurrency",
                                                   namespaces).text.strip() if exchange_rate is not None and exchange_rate.find(
                    "cb:baseCurrency", namespaces) is not None else None
                target_currency = exchange_rate.find("cb:targetCurrency",
                                                     namespaces).text.strip() if exchange_rate is not None and exchange_rate.find(
                    "cb:targetCurrency", namespaces) is not None else None

                if title and description and value and base_currency and target_currency:
                    exchange_rates.append({
                        "title": title,
                        "description": description,
                        "link": link,
                        "date": date,
                        "value": value,
                        "base_currency": base_currency,
                        "target_currency": target_currency,
                    })
            except (AttributeError, ValueError) as e:
                continue

        return exchange_rates

    except requests.exceptions.RequestException as e:
        raise RuntimeError(f"Failed to fetch exchange rates: {e}")
    except etree.XMLSyntaxError as e:
        raise RuntimeError(f"Failed to parse XML response: {e}")
    except Exception as e:
        raise RuntimeError(f"An unexpected error occurred: {e}")


def get_bank_of_canada_cpi_rates():
    url = "https://www.bankofcanada.ca/valet/observations/group/CPI_MONTHLY/json"

    response = requests.get(url)
    response.raise_for_status()
    json_data = response.json()
    return json_data
