from django.contrib import admin

from .models import Organization, CustomUser, Agent

admin.site.register(Organization)
admin.site.register(CustomUser)
admin.site.register(Agent)
