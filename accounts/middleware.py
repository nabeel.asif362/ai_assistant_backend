import logging

from django.utils.deprecation import MiddlewareMixin

logger = logging.getLogger(__name__)


class RequestLogMiddleware(MiddlewareMixin):
    def process_request(self, request):
        logger.info(f"Incoming request: {request.method} {request.get_full_path()}")
        if request.body:
            logger.info(f"Request body: {request.body}")
        return None

    def process_response(self, request, response):
        logger.info(f"Response status: {response.status_code}")
        return response

    def process_exception(self, request, exception):
        logger.error(f"Exception: {exception}")
        return None
