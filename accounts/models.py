from django.contrib.auth.models import AbstractUser
from django.db import models


class Organization(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    ran_id = models.CharField(max_length=30, blank=True, null=True)
    image = models.URLField(default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    reset_password_token = models.CharField(max_length=255, null=True, blank=True)
    reset_password_token_expiration = models.DateTimeField(null=True, blank=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='users', null=True,
                                     blank=True)
    is_first_interaction_with_agent = models.BooleanField(default=True, null=True)
    ROLE_CHOICES = [
        ('owner', 'Owner'),
        ('admin', 'Admin'),
        ('user', 'User'),
        ('superadmin', 'SuperAdmin'),
    ]
    role = models.CharField(max_length=20, choices=ROLE_CHOICES, default='user')
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.email


class Agent(models.Model):
    organization = models.ForeignKey(Organization, related_name='agents', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    customuser = models.ForeignKey(CustomUser, related_name='agents', on_delete=models.CASCADE, default=1)
    text = models.TextField(blank=True, null=True)
    qa = models.TextField(blank=True, null=True)
    ran_id = models.CharField(max_length=30, blank=True, null=True)
    status = models.CharField(max_length=30, default='Untrained')
    model = models.CharField(max_length=30, default='gpt-4o')
    visibility = models.CharField(max_length=30, default='Private')
    temperature = models.FloatField(default=0.5)
    image_url = models.URLField(max_length=500, blank=True, null=True)
    logo_url = models.URLField(max_length=500, blank=True, null=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def update_status(self, new_status):
        if self.status != new_status:
            self.status = new_status
            self.save()

    def __str__(self):
        return self.name


class AgentFile(models.Model):
    FILE_STATUS_CHOICES = [('connected', 'Connected'), ('disconnected', 'Disconnected')]
    FILE_CATEGORY_CHOICES = [('file', 'File'), ('image', 'Image'), ('website', 'Website')]
    WEBSITE_SCRAPE_STATUS_CHOICES = [('failed', 'Failed'), ('success', 'Success')]
    WEBSITE_AUTO_UPDATE_CHOICES = [('manually', 'Manually'), ('daily', 'Daily'), ('weekly', 'Weekly'),
                                   ('monthly', 'Monthly'), ('quarterly', 'Quarterly')]

    agent = models.ForeignKey(Agent, related_name='files', on_delete=models.CASCADE)
    file_name = models.CharField(max_length=255, blank=True, null=True)
    file_url = models.URLField(max_length=500, blank=True, null=True)
    file_characters = models.IntegerField(blank=True, null=True)
    file_status = models.CharField(max_length=30, choices=FILE_STATUS_CHOICES, default='connected', null=True)
    file_category = models.CharField(max_length=30, choices=FILE_CATEGORY_CHOICES, default='file', null=True)
    text_content = models.TextField(blank=True, null=True)
    website_url = models.CharField(max_length=500, blank=True, null=True)
    website_scrape_status = models.CharField(max_length=30, choices=WEBSITE_SCRAPE_STATUS_CHOICES, blank=True,
                                             null=True)
    website_auto_update = models.CharField(max_length=30, choices=WEBSITE_AUTO_UPDATE_CHOICES, blank=True, null=True)
    source_name = models.CharField(max_length=255, blank=True, null=True)
    source_context = models.TextField(blank=True, null=True)
    source_instructions = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return f"{self.agent.name} - File"


class ChatSession(models.Model):
    user = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='chat_sessions'
    )
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE, related_name='chat_sessions')
    title = models.CharField(max_length=255, default='New Chat')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"ChatSession {self.id} - {self.title}"


class Message(models.Model):
    chat_session = models.ForeignKey(
        ChatSession,
        on_delete=models.CASCADE,
        related_name='messages'
    )
    role = models.CharField(max_length=10)
    message = models.TextField()
    sender_name = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Message {self.id} in ChatSession {self.chat_session.id}"


class SourceApiConnections(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    image_url = models.URLField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name or self.description


class AgentSourceApiConnections(models.Model):
    source_api_connection = models.ForeignKey(SourceApiConnections, on_delete=models.CASCADE)
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.source_api_connection} - {self.agent}"


class AgentGraphApiConnections(models.Model):
    AUTO_CALL_CHOICES = [('manually', 'Manually'), ('daily', 'Daily'), ('weekly', 'Weekly'),
                         ('monthly', 'Monthly'), ('quarterly', 'Quarterly')]
    name = models.CharField(max_length=255, blank=True, null=True)
    source_api_connection = models.ForeignKey(SourceApiConnections, on_delete=models.CASCADE,
                                              related_name='agent_graphs')
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE, related_name='agent_graphs', null=True)
    auto_call = models.CharField(max_length=30, choices=AUTO_CALL_CHOICES, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class ExchangeRate(models.Model):
    TAB_TYPE_CHOICES = [('connections', 'Connections'), ('tools', 'Tools')]
    agent_source_api_connection = models.ForeignKey(
        AgentSourceApiConnections,
        on_delete=models.SET_NULL,
        related_name='exchange_rates',
        null=True
    )
    agent_graph_api_connection = models.ForeignKey(
        AgentGraphApiConnections,
        on_delete=models.SET_NULL,
        related_name='exchange_rates',
        null=True
    )
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE, related_name='exchange_rates', null=True)

    title = models.CharField(max_length=255)
    description = models.TextField()
    link = models.URLField()
    date = models.DateTimeField()
    value = models.FloatField()
    base_currency = models.CharField(max_length=10)
    target_currency = models.CharField(max_length=10)

    default_value = models.FloatField(null=True, blank=True)
    default_title = models.CharField(max_length=255, null=True, blank=True)
    default_description = models.TextField(null=True, blank=True)
    default_link = models.URLField(null=True, blank=True)

    is_custom = models.BooleanField(default=False)
    tab_type = models.CharField(max_length=30, choices=TAB_TYPE_CHOICES, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def reset_to_defaults(self):
        if self.default_value is not None:
            self.value = self.default_value
            self.title = self.default_title
            self.description = self.default_description
            self.link = self.default_link
            self.is_custom = False
            self.save()

    def __str__(self):
        return f"{self.base_currency} to {self.target_currency}: {self.value} ({self.date})"


class AgentInstructions(models.Model):
    agent = models.ForeignKey(Agent, related_name='agentinstructions', on_delete=models.CASCADE)
    title = models.CharField(max_length=255, blank=True, null=True)
    instructions = models.TextField()
    is_active = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title or "No Title"
