# Generated by Django 5.0.6 on 2024-07-08 11:18

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0012_organization_ran_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='instructions',
            field=models.TextField(blank=True, null=True),
        ),
    ]
