# Generated by Django 5.0.6 on 2024-12-27 09:42

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0036_alter_customuser_created_at_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='image_url',
            field=models.URLField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='agentfile',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='agentfile',
            name='file_category',
            field=models.CharField(choices=[('file', 'File'), ('image', 'Image'), ('website', 'Website')],
                                   default='file', max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='agentfile',
            name='file_status',
            field=models.CharField(choices=[('connected', 'Connected'), ('disconnected', 'Disconnected')],
                                   default='connected', max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='agentfile',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
