# Generated by Django 5.0.6 on 2024-06-26 10:26

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0007_agent_files_delete_agentfile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agent',
            name='files',
        ),
        migrations.CreateModel(
            name='AgentFile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_url', models.URLField(blank=True, max_length=500, null=True)),
                ('agent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files',
                                            to='accounts.agent')),
            ],
        ),
    ]
