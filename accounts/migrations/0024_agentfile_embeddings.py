# Generated by Django 5.0.6 on 2024-09-27 05:13

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0023_chatmessage_is_valid'),
    ]

    operations = [
        migrations.AddField(
            model_name='agentfile',
            name='embeddings',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), blank=True, null=True,
                                                            size=None),
        ),
    ]
