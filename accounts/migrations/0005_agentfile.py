# Generated by Django 5.0.6 on 2024-06-14 04:17

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0004_remove_agent_email_agent_customuser'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgentFile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_url', models.URLField(blank=True, max_length=500, null=True)),
                ('agent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files',
                                            to='accounts.agent')),
            ],
        ),
    ]
