import io
import os
import random
import re
import string
import tempfile
import uuid
from datetime import timedelta

import fitz  # PyMuPDF
import openai
import pytesseract
import unicodedata
from PIL import Image
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.core.files.storage import default_storage
from django.db import IntegrityError
from django.db.models import Sum
from django.utils.crypto import get_random_string
from django.utils.timezone import now
from docx import Document
from dotenv import load_dotenv
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Organization, Agent, AgentFile, ChatSession, Message, SourceApiConnections, ExchangeRate, \
    AgentInstructions
from .scraper import scrape_content

load_dotenv()
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
openai.api_key = OPENAI_API_KEY
CustomUser = get_user_model()


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, data):
        email = data.get('email').lower()
        password = data.get('password')

        user = authenticate(username=email, password=password)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Invalid credentials")


class ForgotPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        try:
            email = value.lower()
            CustomUser.objects.get(email=email)
        except CustomUser.DoesNotExist:
            raise serializers.ValidationError('Email address not found.')

        return value

    def save(self):
        email = self.validated_data['email'].lower()
        user = CustomUser.objects.get(email=email)
        token = get_random_string(length=32)
        user.reset_password_token = token
        user.reset_password_token_expiration = now() + timedelta(hours=1)
        user.save()
        return token


class ResetPasswordSerializer(serializers.Serializer):
    token = serializers.CharField()
    new_password = serializers.CharField()

    def validate_token(self, value):
        try:
            CustomUser.objects.get(reset_password_token=value, reset_password_token_expiration__gte=now())
        except CustomUser.DoesNotExist:
            raise serializers.ValidationError('Invalid or expired token')
        return value

    def save(self):
        token = self.validated_data['token']
        new_password = self.validated_data['new_password']
        user = CustomUser.objects.get(reset_password_token=token)
        user.set_password(new_password)
        user.reset_password_token = None
        user.reset_password_token_expiration = None
        user.save()


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'first_name', 'email', 'role', 'organization', 'is_first_interaction_with_agent']


class RegisterSerializer(serializers.ModelSerializer):
    organization_name = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)
    first_name = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = ('first_name', 'email', 'password', 'organization_name')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        characters = string.ascii_letters + string.digits + '-_'
        ran_id = ''.join(random.choice(characters) for _ in range(21))

        organization_name = validated_data.pop('organization_name')
        password = validated_data.pop('password')
        first_name = validated_data.pop('first_name')
        email = validated_data['email'].lower()

        try:
            organization = Organization.objects.create(
                name=organization_name,
                email=email,
                ran_id=ran_id
            )

            user = CustomUser(
                email=email,
                first_name=first_name,
                role='owner',
                organization=organization
            )

            user.set_password(password)
            user.save()
            return user

        except IntegrityError as e:
            if 'accounts_organization_email_key' in str(e):
                raise ValidationError({'email': 'A user with this email already exists.'})
            raise e


class AgentFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgentFile
        fields = '__all__'


class GetAgentByIdFilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgentFile
        exclude = ['agent', 'text_content', 'website_scrape_status', 'file_status']


class AgentInstructionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgentInstructions
        fields = ['id', 'title', 'instructions', 'is_active']


class AgentSerializer(serializers.ModelSerializer):
    organization = OrganizationSerializer(read_only=True)
    instructions = AgentInstructionsSerializer(many=True, read_only=True, source='agentinstructions')
    source_files_and_websites = serializers.SerializerMethodField()

    class Meta:
        model = Agent
        fields = '__all__'

    def get_source_files_and_websites(self, obj):
        connected_files = obj.files.filter(file_status='connected')
        return AgentFileSerializer(connected_files, many=True).data


class GetAgentByIdSerializer(serializers.ModelSerializer):
    instructions = AgentInstructionsSerializer(source='agentinstructions', many=True, read_only=True)
    files = GetAgentByIdFilesSerializer(many=True, read_only=True)

    class Meta:
        model = Agent
        exclude = ['customuser', 'organization']


class UpdateAgentSerializer(serializers.ModelSerializer):
    id = serializers.PrimaryKeyRelatedField(queryset=Agent.objects.all())
    name = serializers.CharField(max_length=255, required=False)
    model = serializers.CharField(max_length=30, required=False)
    visibility = serializers.CharField(max_length=30, required=False)
    temperature = serializers.FloatField(required=False, max_value=1.0, min_value=0.0)
    text = serializers.CharField(required=False)
    qa = serializers.CharField(required=False)
    image = serializers.ImageField(required=False)
    logo = serializers.ImageField(required=False)

    class Meta:
        model = Agent
        fields = '__all__'

    def update(self, instance, validated_data):
        def clean_value(val):
            normalized = unicodedata.normalize("NFKC", val)
            cleaned = re.sub(r'^["\']+|["\']+$', '', normalized.strip())
            return cleaned

        if 'name' in validated_data:
            new_name = validated_data.get('name', '').strip()
            if new_name:
                user = instance.customuser
                if Agent.objects.filter(name=new_name, customuser=user).exclude(id=instance.id).exists():
                    raise serializers.ValidationError(
                        {'error_message': f'An agent with the same name already exists.'}
                    )

        for field in ['name', 'model', 'visibility', 'temperature', 'text', 'qa']:
            if field in validated_data:
                value = validated_data.get(field)
                if isinstance(value, str):
                    value = clean_value(value)
                if value == "":
                    value = None
                setattr(instance, field, value)

        # Agent status trained & untrained on text & qa
        if validated_data.get('text') or validated_data.get('qa'):
            instance.update_status('Trained')

        image = validated_data.get('image', None)

        if image:
            unique_filename = f"{uuid.uuid4().hex}_{image.name}"
            s3_file_url = default_storage.save(f'images/{unique_filename}', image)
            file_url = default_storage.url(s3_file_url)
            instance.image_url = file_url

        logo = validated_data.get('logo', None)
        if logo:
            unique_filename = f"{uuid.uuid4().hex}_{logo.name}"
            s3_file_url = default_storage.save(f'images/{unique_filename}', logo)
            file_url = default_storage.url(s3_file_url)
            instance.logo_url = file_url

        instance.save()
        return instance


class ChatSessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatSession
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'role', 'message', 'sender_name', 'created_at']


class FileMetadataSerializer(serializers.Serializer):
    file_id = serializers.PrimaryKeyRelatedField(queryset=AgentFile.objects.filter(file_status='disconnected'))
    source_name = serializers.CharField(required=True, max_length=100)
    source_context = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    source_instructions = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    def validate(self, data):
        if not AgentFile.objects.filter(id=data["file_id"].id, file_status="disconnected").exists():
            raise serializers.ValidationError(
                {"file_id": f"File with id {data['file_id']} is not valid or already connected."}
            )
        return data

    def create(self, validated_data):
        file_instance = validated_data['file_id']
        file_instance.source_name = validated_data['source_name']
        file_instance.source_context = validated_data['source_context']
        file_instance.source_instructions = validated_data['source_instructions']
        file_instance.file_status = 'connected'

        agent = Agent.objects.get(id=file_instance.agent.id)
        agent.update_status('Trained')

        file_instance.save()

        return file_instance


class ImageMetadataSerializer(serializers.Serializer):
    file_id = serializers.PrimaryKeyRelatedField(
        queryset=AgentFile.objects.filter(file_status='disconnected', file_category='image'))
    source_name = serializers.CharField(required=True, max_length=100)
    source_context = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    source_instructions = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    def validate(self, data):
        if not AgentFile.objects.filter(id=data["file_id"].id, file_status="disconnected",
                                        file_category="image").exists():
            raise serializers.ValidationError(
                {"file_id": f"Image with id {data['file_id']} is not valid or already connected."}
            )
        return data

    def create(self, validated_data):
        image_instance = validated_data['file_id']
        image_instance.source_name = validated_data['source_name']
        image_instance.source_context = validated_data['source_context']
        image_instance.source_instructions = validated_data['source_instructions']
        image_instance.file_status = 'connected'

        agent = Agent.objects.get(id=image_instance.agent.id)
        agent.update_status('Trained')

        image_instance.save()

        return image_instance


class TrainAgentByWebsitesSerializer(serializers.ModelSerializer):
    agent_id = serializers.PrimaryKeyRelatedField(queryset=Agent.objects.all())
    website_url = serializers.URLField(required=True)
    website_auto_update = serializers.CharField(required=True)
    source_name = serializers.CharField(required=True, max_length=100)
    source_context = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    source_instructions = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = AgentFile
        fields = ['agent_id', 'website_url', 'website_auto_update', 'source_name', 'source_context',
                  'source_instructions']

    def create(self, validated_data, website_text=None):

        agent = validated_data['agent_id']
        website_url = validated_data['website_url']
        website_auto_update = validated_data['website_auto_update']
        source_name = validated_data['source_name']
        source_context = validated_data['source_context']
        source_instructions = validated_data['source_instructions']

        try:
            response = scrape_content(website_url)
            website_data = {"website_headers": response["headers"], "website_text": response["cleaned_text"],
                            "website_tables": response["tables"]}

            text_length = len(response["cleaned_text"])
            tables_length = sum(len(str(table)) for table in response["tables"])

            if not response["headers"] and not response["cleaned_text"] and not response["tables"]:
                raise ValueError("Due to website restrictions, scrapping is not possible")

            if text_length < 100 and tables_length < 100:
                raise ValueError("Due to website restrictions, scrapping is not possible")

            agent.update_status('Trained')
            AgentFile.objects.create(agent=agent,
                                     file_characters=response["characters_count"], file_category='website',
                                     text_content=website_data, website_url=website_url,
                                     website_scrape_status='success', website_auto_update=website_auto_update,
                                     source_name=source_name, source_context=source_context,
                                     source_instructions=source_instructions, )

            return {"website_url": website_url, "characters_count": response["characters_count"],
                    "error_message": response["error_message"]}
        except Exception as e:
            return {"website_url": website_url, "characters_count": None,
                    "error_message": "Due to website restrictions, scrapping is not possible."}


class SourceCharacterCounterSerializer(serializers.Serializer):
    agent_id = serializers.IntegerField(required=True)
    files = serializers.ListField(child=serializers.FileField(), write_only=True, required=False)
    images = serializers.ListField(child=serializers.ImageField(), write_only=True, required=False)

    def create(self, validated_data):
        agent_instance = self.get_agent_instance(validated_data['agent_id'])
        files = validated_data.get('files', [])
        images = validated_data.get('images', [])
        agent_character_limit = 400000
        agent_existing_characters = self.get_existing_character_count(agent_instance)
        results = []

        for file in files:
            result = self.process_file(file, agent_instance, agent_existing_characters, agent_character_limit)
            results.append(result)
            agent_existing_characters += result["characters_count"]
            if agent_existing_characters >= agent_character_limit:
                break

        for image in images:
            result = self.process_image(image, agent_instance, agent_existing_characters, agent_character_limit)
            results.append(result)
            agent_existing_characters += result["characters_count"]
            if agent_existing_characters >= agent_character_limit:
                break

        return results

    def get_agent_instance(self, agent_id):
        try:
            return Agent.objects.get(id=agent_id)
        except Agent.DoesNotExist:
            raise serializers.ValidationError("Invalid agent_id provided.")

    def get_existing_character_count(self, agent_instance):
        return AgentFile.objects.filter(agent=agent_instance).aggregate(total=Sum('file_characters'))['total'] or 0

    def process_file(self, file, agent_instance, agent_existing_characters, agent_character_limit):
        file_name = file.name.lower()
        file_content = file.read()
        unique_filename = f"{uuid.uuid4().hex}_{file.name}"
        file_url = default_storage.url(default_storage.save(f'files/{unique_filename}', file))

        if file_name.endswith('.pdf'):
            return self.process_pdf(file.name, file_content, file_url, agent_instance, agent_existing_characters,
                                    agent_character_limit)
        elif file_name.endswith('.txt'):
            return self.process_text_file(file.name, file_content, file_url, agent_instance, agent_existing_characters,
                                          agent_character_limit)
        elif file_name.endswith('.docx'):
            return self.process_docx(file.name, file_content, file_url, agent_instance, agent_existing_characters,
                                     agent_character_limit)
        else:
            return {"file_id": None, "file_name": file.name, "file_url": None, "characters_count": 0,
                    "error_message": "Unsupported file type."}

    def process_image(self, image, agent_instance, agent_existing_characters, agent_character_limit):
        try:
            unique_filename = f"{uuid.uuid4().hex}_{os.path.basename(image.name)}"

            file_url = default_storage.url(default_storage.save(f'images/{unique_filename}', image))

            image.seek(0)

            with tempfile.NamedTemporaryFile(delete=True, suffix=os.path.splitext(image.name)[1]) as temp_file:
                temp_file.write(image.read())
                temp_file.flush()
                img = Image.open(temp_file.name)

                img = img.convert("L")
                img = img.resize((int(img.width * 1.5), int(img.height * 1.5)))
                text_from_img = pytesseract.image_to_string(img).strip()

            if not text_from_img:
                return {
                    "file_id": None,
                    "file_name": image.name,
                    "file_url": file_url,
                    "characters_count": 0,
                    "error_message": "No information is obtained from this image."
                }

            if agent_existing_characters + len(text_from_img) >= agent_character_limit:
                return {
                    "file_id": None,
                    "file_name": image.name,
                    "file_url": file_url,
                    "characters_count": len(text_from_img),
                    "error_message": "This file has reached the agent limit, please try with a smaller file."
                }

            agent_file = AgentFile.objects.create(agent=agent_instance, file_name=image.name, file_url=file_url,
                                                  file_status='disconnected',
                                                  file_category='image',
                                                  text_content=text_from_img,
                                                  file_characters=len(text_from_img))

            return {
                "file_id": agent_file.id,
                "file_name": image.name,
                "file_url": file_url,
                "characters_count": len(text_from_img),
                "error_message": None
            }
        except Exception as e:
            print(f"Error processing image: {e}")
            return {
                "file_id": None,
                "file_name": image.name,
                "file_url": None,
                "characters_count": 0,
                "error_message": "Please provide a valid image with text content."
            }

    def process_pdf(self, file_name, file_content, file_url, agent_instance, agent_existing_characters,
                    agent_character_limit):
        pdf_document = fitz.open(stream=file_content, filetype="pdf")
        pdf_text = "".join([page.get_text() for page in pdf_document])

        return self.finalize_file_processing(file_name, file_url, agent_instance, pdf_text, agent_existing_characters,
                                             agent_character_limit)

    def process_text_file(self, file_name, file_content, file_url, agent_instance, agent_existing_characters,
                          agent_character_limit):
        text_content = file_content.decode('utf-8')
        return self.finalize_file_processing(file_name, file_url, agent_instance, text_content,
                                             agent_existing_characters, agent_character_limit)

    def process_docx(self, file_name, file_content, file_url, agent_instance, agent_existing_characters,
                     agent_character_limit):
        doc = Document(io.BytesIO(file_content))
        doc_text = "\n".join([para.text for para in doc.paragraphs if para.text.strip()])
        return self.finalize_file_processing(file_name, file_url, agent_instance, doc_text, agent_existing_characters,
                                             agent_character_limit)

    def finalize_file_processing(self, file_name, file_url, agent_instance, text, agent_existing_characters,
                                 agent_character_limit):
        if agent_existing_characters + len(text) >= agent_character_limit:
            return {
                "file_id": None,
                "file_name": file_name,
                "file_url": file_url,
                "characters_count": len(text),
                "error_message": "This file has reached the agent limit, please try with a smaller file."
            }

        agent_file = AgentFile.objects.create(agent=agent_instance, file_name=file_name, file_url=file_url,
                                              file_category='file',
                                              text_content=text,
                                              file_characters=len(text),
                                              file_status='disconnected')
        return {
            "file_id": agent_file.id,
            "file_name": file_name,
            "file_url": file_url,
            "characters_count": len(text),
            "error_message": None
        }


class UpdateOrganizationLogoSerializer(serializers.ModelSerializer):
    image = serializers.FileField(required=True)

    class Meta:
        model = Organization
        fields = '__all__'

    def update(self, instance, validated_data):
        try:
            image = validated_data['image']
        except Exception as e:
            raise serializers.ValidationError(f"Missing {e} key from request data")

        unique_filename = f"{uuid.uuid4().hex}_{image.name}"

        if instance.image:
            try:
                file_path = instance.image.replace(os.getenv("AWS_S3_BUCKET_BASE_URL"), "")
                default_storage.delete(file_path)
            except Exception as e:
                raise serializers.ValidationError(f"Error deleting old image: {e}")

        try:
            s3_file_url = default_storage.save(f'organization/logo/{unique_filename}', image)
            file_url = default_storage.url(s3_file_url)
            instance.image = file_url
            instance.save()

        except Exception as e:
            raise serializers.ValidationError(f"Error uploading new image: {e}")

        return instance


class UpdateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['first_name', 'is_first_interaction_with_agent']


class ChangePasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_current_password(self, value):
        user = self.context['request'].user

        if not user.check_password(value):
            raise serializers.ValidationError("Current password is incorrect.")

        return value

    def validate_new_password(self, value):
        if len(value) < 8:
            raise serializers.ValidationError("The new password must be at least 8 characters long.")

        return value


class SourceApiConnectionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SourceApiConnections
        fields = '__all__'


class ExchangeRateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExchangeRate
        fields = ['title', 'base_currency', 'target_currency', 'value']

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        instance.is_custom = True
        instance.save()
        return instance


class ExchangeRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExchangeRate
        fields = '__all__'
