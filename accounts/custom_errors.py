class CustomError(Exception):
    def __init__(self, error_data):
        super().__init__(error_data)
        self.error_data = error_data
