import pandas as pd
import requests
from bs4 import BeautifulSoup
from django.apps import apps


def preprocess_text(text):
    try:
        nlp = apps.get_app_config('accounts').nlp
        doc = nlp(text)
        processed_sentences = []
        for sent in doc.sents:
            processed_sentences.append(' '.join([token.text for token in sent if token.is_alpha or token.is_stop]))
        return ' '.join(processed_sentences)
    except Exception as e:
        raise ValueError(f"Error during text preprocessing: {str(e)}")


def calculate_total_characters(cleaned_text, table_dataframes, headers):
    total_characters = len(cleaned_text)
    for df in table_dataframes:
        total_characters += len(
            df.to_string(index=False, header=True))
    for tag, header_list in headers.items():
        total_characters += sum(len(header) for header in header_list)
    return total_characters


def scrape_content(url):
    try:
        try:
            response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'}, timeout=10)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            return {"error_message": f"Failed to fetch the webpage: {str(e)}",
                    "status_code": getattr(e.response, 'status_code', None)}

        try:
            soup = BeautifulSoup(response.text, 'lxml')
        except Exception as e:
            return {"error_message": f"Failed to parse the webpage content: {str(e)}", "status_code": None}

        try:
            text_content = ' '.join(soup.stripped_strings)
        except Exception as e:
            return {"error_message": f"Failed to extract text content: {str(e)}", "status_code": None}

        tables = []
        try:
            for table in soup.find_all('table'):
                rows = []
                for tr in table.find_all('tr'):
                    cells = [td.get_text(strip=True) for td in tr.find_all(['td', 'th'])]
                    rows.append(cells)
                tables.append(rows)
        except Exception as e:
            return {"error_message": f"Failed to extract tables: {str(e)}", "status_code": None}

        try:
            headers = {tag: [el.get_text(strip=True) for el in soup.find_all(tag)] for tag in ['h1', 'h2', 'h3']}
        except Exception as e:
            return {"error_message": f"Failed to extract headers: {str(e)}", "status_code": None}

        try:
            cleaned_text = preprocess_text(text_content)
        except ValueError as e:
            return {"error_message": f"Failed to process text with spaCy: {str(e)}", "status_code": None}

        try:
            table_dataframes = [pd.DataFrame(table) for table in tables]
        except Exception as e:
            return {"error_message": f"Failed to convert tables to DataFrames: {str(e)}", "status_code": None}

        try:
            total_characters = calculate_total_characters(cleaned_text, table_dataframes, headers)
        except Exception as e:
            return {"error_message": f"Failed to calculate total characters: {str(e)}", "status_code": None}

        return {
            "cleaned_text": cleaned_text,
            "tables": table_dataframes,
            "headers": headers,
            "characters_count": total_characters,
            "error_message": None
        }

    except Exception as e:
        return {"error_message": f"An unexpected error occurred: {str(e)}", "status_code": None}
