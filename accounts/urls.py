from django.urls import path

from .views import RegisterView, AgentCreateView, SuperAdminView, LoginView, ForgotPasswordView, ResetPasswordView, \
    GetAgentsView, DeleteAgentByIdView, UpdateAgentView, UpdateAgentFileByIdView, MyOrganizationView, \
    DeleteOrganizationView, \
    GetAgentChatsView, DeleteAgentFileByIdView, DeleteAgentChatByIdView, \
    GetChatSessionMessagesView, RenameChatSessionView, GetAgentByIdView, TrainAgentByFilesView, \
    UpdateOrganizationLogoView, UserProfileView, ChangePasswordView, SourceCharacterCounterView, TrainAgentByImagesView, \
    TrainAgentByWebsitesView, SourceApiConnectionsView, ConnectSourceApiView, DisconnectSourceApiView, \
    RecentExchangeRatesView, UpdateExchangeRateView, ResetExchangeRatesView, DashboardConnectView, \
    DashboardGraphApiView, DashboardDisconnectView, DashboardGraphDataView, ResetGraphDataView, DashboardResumeView, \
    facebook_webhook, UpdateAgentInstructionByIdView

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('forgot_password/', ForgotPasswordView.as_view(), name='forgot-password'),
    path('reset_password/', ResetPasswordView.as_view(), name='reset-password'),
    path('superadmin/', SuperAdminView.as_view(), name='superadmin-view'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('agents/create/', AgentCreateView.as_view(), name='agent-create'),
    path('agents/', GetAgentsView.as_view(), name='get-agents'),
    path('agents/<int:agent_id>/', GetAgentByIdView.as_view(), name='get-agents'),
    path('agents/delete/<int:agent_id>/', DeleteAgentByIdView.as_view(), name='delete-agent-by-id'),
    path('agents/update/', UpdateAgentView.as_view(), name='update-agent'),
    path('agents/files/update/<int:file_id>/', UpdateAgentFileByIdView.as_view(), name='update-agent-file'),
    path('agents/files/delete/<int:file_id>/', DeleteAgentFileByIdView.as_view(), name='delete-agent-file'),
    path('agents/instructions/update/<int:instruction_id>/', UpdateAgentInstructionByIdView.as_view(),
         name='update-agent-instruction'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('agents/train_files/', TrainAgentByFilesView.as_view(), name='train-agent-with-files'),
    path('agents/train_images/', TrainAgentByImagesView.as_view(), name='train-agent-with-images'),
    path('agents/<int:agent_id>/train_websites/', TrainAgentByWebsitesView.as_view(), name='train-agent-with-websites'),
    path('agents/<int:agent_id>/sources/character_counter/', SourceCharacterCounterView.as_view(),
         name='source-character-counter'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('agents/<int:agent_id>/source_api_connections/', SourceApiConnectionsView.as_view(),
         name='get-source-api-connections'),
    path('agents/<int:agent_id>/source_api_connections/<int:source_id>/connect/', ConnectSourceApiView.as_view(),
         name='connect-source-api'),
    path('agents/<int:agent_id>/source_api_connections/<int:source_id>/disconnect/', DisconnectSourceApiView.as_view(),
         name='disconnect-source-api'),
    path('agents/<int:agent_id>/source_api_connections/<int:agent_source_api_connection_id>/exchange_rates/',
         RecentExchangeRatesView.as_view(),
         name='recent-fx-api'),
    path('agents/source_api_connections/exchange_rates/<int:rate_id>/',
         UpdateExchangeRateView.as_view(),
         name='update-exchange-rate-api'),
    path('agents/<int:agent_id>/source_api_connections/<int:agent_source_api_connection_id>/exchange_rates/reset/',
         ResetExchangeRatesView.as_view(),
         name='reset-exchange-rate-api'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('agents/<int:agent_id>/tools/dashboard/graph_api_connections/', DashboardGraphApiView.as_view(),
         name='dashboard-graph-api-view'),
    path('agents/<int:agent_id>/tools/dashboard/graph_api_connections/<int:source_id>/connect/',
         DashboardConnectView.as_view(),
         name='dashboard-graph-api-connect'),
    path('agents/<int:agent_id>/tools/dashboard/graph_api_connections/<int:source_id>/disconnect/',
         DashboardDisconnectView.as_view(),
         name='dashboard-graph-api-disconnect'),
    path('agents/<int:agent_id>/tools/dashboard/graph_api_connections/data/<int:agent_graph_api_connection_id>/',
         DashboardGraphDataView.as_view(),
         name='dashboard-graph-data-view'),
    path('agents/<int:agent_id>/tools/dashboard/graph_api_connections/<int:agent_graph_api_connection_id>/reset/',
         ResetGraphDataView.as_view(),
         name='dashboard-graph-data-reset-view'),

    path('agents/<int:agent_id>/tools/resume/report/', DashboardResumeView.as_view(),
         name='dashboard-resume-view'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('org/my/', MyOrganizationView.as_view(), name='get-my-org'),
    path('org/logo/', UpdateOrganizationLogoView.as_view(), name='update-org-logo'),
    path('org/delete/<int:org_id>/', DeleteOrganizationView.as_view(), name='delete-organization'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('chats/<int:agent_id>/', GetAgentChatsView.as_view(), name='get-agent-chats'),
    path('chats/update/<int:chat_session_id>/', RenameChatSessionView.as_view(), name='rename-chat-session'),
    path('chats/delete/<int:chat_session_id>/', DeleteAgentChatByIdView.as_view(), name='delete-agent-chats'),
    path('chat_session_messages/<int:chat_session_id>/', GetChatSessionMessagesView.as_view(),
         name='get-chat-session-messages'),
    path('chat_session_messages/<str:chat_session_id>/', GetChatSessionMessagesView.as_view(),
         name='get-chat-session-messages-string'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('user/profile/', UserProfileView.as_view(), name='user-profile'),
    path('user/change_password/', ChangePasswordView.as_view(), name='change-password'),

    #   ------------------------------------------------------------------------------------------------------------   #

    path('webhook/facebook', facebook_webhook, name='facebook_webhook'),

    #   ------------------------------------------------------------------------------------------------------------   #
]
