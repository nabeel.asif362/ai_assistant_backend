# Django Backend Application

This is an AI Assistant Backend Application. The project is designed to serve as a RESTful API for web or mobile
frontends.

---

## Installation and Setup

### 1. Windows Installation

#### Prerequisites

- Python 3.12.3 or higher
- pip (Python package installer)

#### Clone the Repository

```bash
  git clone git@gitlab.com:nabeel.asif362/ai_assistant_backend.git
  cd ai_assistant_backend
```

#### Create a Virtual Environment

```bash
  python -m venv .venv
  .venv\Scripts\activate
```

#### Install Dependencies

```bash
  pip install -r requirements.txt
  python -m spacy download en_core_web_md
```

#### Configuration

Create a `.env` file in the root directory to store environment-specific settings:

```
SECRET_KEY=
DEBUG=
ALLOWED_HOSTS=
PASSWORD_RESET_URL=

DATABASE_NAME=
DATABASE_USER=
DATABASE_PORT=
DATABASE_HOST=
DATABASE_PASSWORD=

OPENAI_API_KEY=

SUPER_ADMIN_USERNAME=
SUPER_ADMIN_PASSWORD=

EMAIL_BACKEND=
EMAIL_HOST=
EMAIL_PORT=
EMAIL_USE_TLS=
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
DEFAULT_FROM_EMAIL=

AWS_ACCESS_KEY=
AWS_SECRET_ACCESS_KEY=
AWS_S3_BUCKET_BASE_URL=
AWS_STORAGE_BUCKET_NAME=
AWS_S3_REGION_NAME=

# For Windows
TESSERACT_PATH=C:/Program Files/Tesseract-OCR
```

#### Database Setup

```bash
  python manage.py makemigrations
  python manage.py migrate
```

#### Create a Superuser

```bash
  python manage.py createsuperuser
```

#### Run the Server

```bash
  python manage.py runserver
```

---

### 2. Ubuntu Installation & Deployment

#### Setup Your Server

```bash
  sudo apt update -y && sudo apt upgrade -y 
```

#### Install Required Packages

```bash
  sudo apt install python3 python3-pip python3-venv tesseract-ocr ffmpeg
```

#### Setup Django Project

```bash 
  cd /path/to/your/django/project
  python3 -m venv .venv
  source .venv/bin/activate
  pip install -r requirements.txt
  python3 -m spacy download en_core_web_md
```

#### Configuration

Create a `.env` file in the root directory to store environment-specific settings (same as for Windows).

---

### Deploy with Gunicorn and Nginx

#### Install Gunicorn

```bash
  pip install gunicorn
```

#### Configure Gunicorn

Create a systemd service file for Gunicorn:

```bash
  sudo vim /etc/systemd/system/gunicorn.service
```

Add the following content:

```ini
[Unit]
Description = Gunicorn daemon for Django project
After = network.target

[Service]
User = your_ubuntu_username
Group = www-data
WorkingDirectory = /path/to/your/django/project
ExecStart = /path/to/your/django/project/.venv/bin/gunicorn --workers 3 --bind unix:/path/to/your/django/project/gunicorn.sock your_project.wsgi:application

[Install]
WantedBy = multi-user.target
```

#### Start and Enable Gunicorn

```bash
  sudo systemctl daemon-reload
  sudo systemctl start gunicorn
  sudo systemctl enable gunicorn
```

#### Check Gunicorn Status

```bash
  sudo systemctl status gunicorn
```

#### View Logs

- Django Logs:
  ```bash
    tail -f django_request_logging.log
  ```
- Gunicorn Logs:
  ```bash
    sudo journalctl -u gunicorn -f
  ```

---

### Configure Nginx

#### Install Nginx

```bash
  sudo apt install nginx
```

#### Configure Nginx

Create a configuration file for your project:

```bash
  sudo vim /etc/nginx/sites-available/your_project
```

Add the following configuration:

```nginx
server {
    server_name your_server_domain_or_IP;

    location / {
        proxy_pass http://unix:/path/to/your/django/project/gunicorn.sock;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Enable the Nginx configuration:

```bash
  sudo ln -s /etc/nginx/sites-available/your_project /etc/nginx/sites-enabled
```

Test the configuration and restart Nginx:

```bash
  sudo nginx -t
  sudo systemctl restart nginx
```

---

### Install SSL with Certbot

```bash
  sudo apt install certbot python3-certbot-nginx
  sudo certbot --nginx -d your_server_domain_or_IP
```

---

### Restart Services

```bash
  sudo systemctl restart gunicorn
  sudo systemctl restart nginx
```

### 3. Setting Up Django-Cron Job on Ubuntu

This section outlines the steps performed on Ubuntu to configure the Django-Cron job for your AI Assistant application.

#### 1. Edit the Crontab

```bash
  crontab -e
```

This command opens the system crontab for the current user.

---

#### 2. Add Cron Job Entry

Add the following line to the crontab file:

```
* * * * * /path/to/your/venv/bin/python /path/to/your/project/manage.py runcrons >> /path/to/your/project/cron_log.txt 2>&1
```

**Notes:**
- Use `which python` to find the path to your Python virtual environment.
- Use `pwd` to get the path to your Django project.
- This runs the cron job every minute, while Django-Cron internally manages the frequency based on the `RUN_EVERY_MINS` value.
- All output is logged to `cron_log.txt` for debugging purposes.

---

#### 3. Verify Cron Service Status

Ensure the cron service is active by running:

```bash
  sudo service cron status
```

If it's inactive, start the service with:

```bash
  sudo service cron start
```

---

#### 4. Check Logs
To verify the cron job execution, check the output log file:
```bash
  cat /path/to/your/project/cron_log.txt
```
---