import uuid
from datetime import datetime

from django.db import transaction
from django_cron import CronJobBase, Schedule

from accounts.models import ExchangeRate, AgentSourceApiConnections, AgentGraphApiConnections
from accounts.utils import get_bank_of_canada_exchange_rates


class DailyExchangeRateCron(CronJobBase):
    RUN_EVERY_MINS = 1440  # 1440 minutes = 1 day
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = f"ai_assistant.daily_exchange_rate_cron_{uuid.uuid4()}"  # A unique code

    def do(self):
        current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(f"[{current_time}] Starting Daily Exchange Rate Cron Job...")

        try:
            exchange_rates = get_bank_of_canada_exchange_rates()
            if not exchange_rates:
                print(f"[{current_time}] No new exchange rates found.")
                return

            agent_source_connections = AgentSourceApiConnections.objects.filter(source_api_connection__id=1)
            print(
                f"[{current_time}] Found {agent_source_connections.count()} AgentSourceApiConnections for source ID 1.")

            for connection in agent_source_connections:
                agent = connection.agent
                with transaction.atomic():
                    for rate in exchange_rates:
                        base_currency = rate["base_currency"]
                        target_currency = rate["target_currency"]
                        rate_date = rate["date"]

                        existing_rate = ExchangeRate.objects.filter(
                            agent=agent,
                            base_currency=base_currency,
                            target_currency=target_currency,
                            date=rate_date,
                            tab_type='connections'
                        ).first()

                        if not existing_rate:
                            ExchangeRate.objects.create(
                                agent_source_api_connection=connection,
                                agent=agent,
                                title=rate["title"],
                                description=rate.get("description", ""),
                                link=rate.get("link", ""),
                                date=rate_date,
                                value=rate["value"],
                                base_currency=base_currency,
                                target_currency=target_currency,
                                default_value=rate["value"],
                                default_title=rate["title"],
                                default_description=rate.get("description", ""),
                                default_link=rate.get("link", ""),
                                tab_type='connections'
                            )

            agent_graph_connections = AgentGraphApiConnections.objects.filter(source_api_connection__id=1)
            print(f"[{current_time}] Found {agent_graph_connections.count()} AgentGraphApiConnections for source ID 1.")

            for connection in agent_graph_connections:
                agent = connection.agent
                with transaction.atomic():
                    for rate in exchange_rates:
                        base_currency = rate["base_currency"]
                        target_currency = rate["target_currency"]
                        rate_date = rate["date"]

                        existing_rate = ExchangeRate.objects.filter(
                            agent=agent,
                            base_currency=base_currency,
                            target_currency=target_currency,
                            date=rate_date,
                            tab_type='tools'
                        ).first()

                        if not existing_rate:
                            ExchangeRate.objects.create(
                                agent_graph_api_connection=connection,
                                agent=agent,
                                title=rate["title"],
                                description=rate.get("description", ""),
                                link=rate.get("link", ""),
                                date=rate_date,
                                value=rate["value"],
                                base_currency=base_currency,
                                target_currency=target_currency,
                                default_value=rate["value"],
                                default_title=rate["title"],
                                default_description=rate.get("description", ""),
                                default_link=rate.get("link", ""),
                                tab_type='tools'
                            )

            print(
                f"[{current_time}] Daily exchange rates updated successfully for both Source and Graph API connections.")

        except Exception as e:
            print(f"[{current_time}] Exception occurred: {e}")
