from django.urls import path

from .views import ProcessAudioView, ProcessTextView

urlpatterns = [
    path('process_audio/', ProcessAudioView.as_view(), name='process_audio'),
    path('process_text/', ProcessTextView.as_view(), name='process_text'),
]
