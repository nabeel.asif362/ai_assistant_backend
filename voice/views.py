import os
import re
import subprocess
from io import BytesIO

import openai
import speech_recognition as sr
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.utils.timezone import now
from dotenv import load_dotenv
from pydub import AudioSegment
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import Agent, Message, ChatSession, AgentSourceApiConnections, ExchangeRate
from accounts.serializers import AgentSerializer

load_dotenv()
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
openai.api_key = OPENAI_API_KEY

recognizer = sr.Recognizer()


def is_ffmpeg_installed():
    try:
        subprocess.run(["ffmpeg", "-version"], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError:
        return False


def get_agent_data(agent_id):
    agent_instance = get_object_or_404(Agent, id=agent_id)
    agent_data = AgentSerializer(agent_instance).data
    return agent_instance, agent_data


def get_chat_context(chat_session_id):
    """Retrieve the last 10 valid chat messages for the given agent and user."""
    return list(
        Message.objects.filter(chat_session=chat_session_id)
        .order_by('-id')[:10]
        .values('id', 'message', 'role')
    )[::-1]


def create_chat_message(agent, user, message, chat_session_id, role, sender_name):
    if chat_session_id:
        try:
            chat_session = ChatSession.objects.get(id=chat_session_id)
        except ChatSession.DoesNotExist:
            raise ValueError(f"ChatSession with id {chat_session_id} does not exist.")
    else:
        chat_session = ChatSession.objects.create(user=user, agent=agent, title=message)

    Message.objects.create(
        chat_session=chat_session,
        role=role,
        message=message,
        sender_name=sender_name,
    )

    chat_session.updated_at = now()
    chat_session.save(update_fields=['updated_at'])

    return chat_session.id


def get_exchange_rates_for_user(agent):
    try:
        source = AgentSourceApiConnections.objects.get(agent=agent)
        exchange_rates = ExchangeRate.objects.filter(agent_source_api_connection=source.id)
        return exchange_rates

    except ObjectDoesNotExist as e:
        return ExchangeRate.objects.none()


def generate_system_content(agent, chat_history, exchange_rates):
    """Generate system content for OpenAI with agent and chat context."""
    return (
        "### Instructions:\n"
        "1. Do not mention anywhere that you are developed by OpenAI.\n"
        "2. Never use external information or assumptions when answering.\n"
        "3. Always base your response on the provided data sources:\n"
        "   - Agent Details (including text, QA, and instructions fields)\n"
        "   - FX Currency Data\n"
        "   - Chat History\n"
        "4. Processing Steps:\n"
        "   - First, search the provided files (Agent Details, FX Currency Data) for relevant information.\n"
        "   - Pay close attention to the agent object fields:\n"
        "     - **text**: Use this for general content, instructions and information.\n"
        "     - **qa**: Use this for question-answer pairs; refer to this field when responding to direct questions and instructions.\n"
        "     - **file source / context / instructions**: Follow these explicitly when generating responses to ensure accuracy and compliance.\n"
        "5. Always analyze the content deeply before forming your response:\n"
        "- If the user asks questions such as `What do you know about XYZ?`, interpret it as a request to refer to "
        "the provided files and extract relevant information accordingly.\n"
        "6. For questions about currency information or conversion:\n"
        "   - Check the provided FX Currency Data.\n"
        "   - If relevant information is found, respond with the data.\n"
        "   - If not found, say 'Not available right now.'\n"
        "7. Maintain context and continuity of the current conversation using the provided Chat History.\n"
        "   - Ensure responses align with the ongoing discussion.\n"
        "8. Mathematical Formulas:\n"
        "   - Use valid KaTeX syntax for mathematical formulas.\n"
        "   - Block-level formulas should be wrapped in `$$` on both sides for proper rendering.\n"
        "   - Inline formulas should be wrapped in single `$` on both sides.\n"
        "   - Replace double backslashes (`\\\\`) with single backslashes (`\\`) for KaTeX compatibility.\n"
        "\n"
        "### Data:\n\n\n"
        f"Agent Details: {agent}\n\n"
        f"FX Currency Data: {exchange_rates}\n\n"
        "### Chat History (Last 10 Messages):\n\n"
        f"{chat_history}\n"
    )


def optimize_response(response):
    """Clean up and optimize the GPT response by removing special characters and adjusting formatting."""
    response = re.sub(r'[!@#$%^&*()_\-+=\[\]{}|\\:;"\'<>/~`]', '', response)
    response = re.sub(r'\s+', ' ', response)
    response = re.sub(r'(\d\.) ([a-z])', lambda m: f'{m.group(1)} {m.group(2).upper()}', response)
    return response.strip()


def handle_openai_response(agent_data, user, user_messages, agent_instance, chat_session_id, sender_name):
    """Generate a response using the OpenAI GPT model and create a chat message."""
    try:
        gpt_response = openai.ChatCompletion.create(
            model=agent_data['model'],
            messages=user_messages,
            n=1,
            stop=None,
            temperature=agent_data['temperature']
        )
        return gpt_response.choices[0].message['content']
    except openai.error.RateLimitError as e:
        print('e = ', e)
        create_chat_message(agent_instance, user, 'Unable to generate response due to rate limit!', chat_session_id,
                            'agent', sender_name)
        return None
    except openai.error.InvalidRequestError as e:
        if "maximum context length" in str(e):
            create_chat_message(agent_instance, user, "Token limit reached; please refine and reduce the input data.",
                                chat_session_id, "agent", sender_name)
        return None


class ProcessAudioView(APIView):
    parser_classes = [MultiPartParser, FormParser]

    def post(self, request, *args, **kwargs):
        if not is_ffmpeg_installed():
            return Response({'error': 'ffmpeg is not installed or not found in PATH'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        audio_file = request.FILES.get('audio')
        if not audio_file:
            return Response({'error': 'Audio file is required'}, status=status.HTTP_400_BAD_REQUEST)

        agent_id = request.data.get('agent_id')
        chat_session_id = request.data.get('chat_session_id')

        if not agent_id:
            return Response({'error': 'Agent ID is required'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            audio = AudioSegment.from_file(audio_file)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

        wav_io = BytesIO()
        audio.export(wav_io, format='wav')
        wav_io.seek(0)

        with sr.AudioFile(wav_io) as source:
            audio_data = recognizer.record(source)
            try:
                text = recognizer.recognize_google(audio_data)
            except sr.RequestError:
                return Response({'error': 'API unavailable'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            except sr.UnknownValueError:
                return Response({'error': 'Unable to recognize speech'}, status=status.HTTP_400_BAD_REQUEST)

        agent_instance, agent_data = get_agent_data(agent_id)

        chat_context = None

        if chat_session_id == "null":
            chat_session_id = None

        if chat_session_id:
            chat_context = get_chat_context(chat_session_id)

        cs_id = create_chat_message(agent_instance, request.user, text, chat_session_id, 'user',
                                    request.user.first_name)
        exchange_rates = get_exchange_rates_for_user(agent_instance)
        system_content = generate_system_content(agent_data, chat_context, exchange_rates)

        user_messages = [
            {"role": "user", "content": text},
            {"role": "system", "content": system_content}
        ]

        response_message = handle_openai_response(agent_data, request.user, user_messages, agent_instance, cs_id,
                                                  request.user.first_name)

        if response_message is None:
            return Response({'error': 'Rate limit reached'}, status=status.HTTP_429_TOO_MANY_REQUESTS)

        cleaned_response = optimize_response(response_message)

        create_chat_message(agent_instance, request.user, response_message, cs_id, 'agent', agent_instance.name)

        return Response({'response': cleaned_response, 'chat_session_id': cs_id})


class ProcessTextView(APIView):
    def post(self, request, *args, **kwargs):
        text_input = request.data.get('text_input')
        agent_id = request.data.get('agent_id')
        chat_session_id = request.data.get('chat_session_id')

        if not text_input:
            return Response({'error': 'Text input is required'}, status=status.HTTP_400_BAD_REQUEST)
        if not agent_id:
            return Response({'error': 'Agent ID is required'}, status=status.HTTP_400_BAD_REQUEST)

        agent_instance, agent_data = get_agent_data(agent_id)

        chat_context = None

        if chat_session_id == "null":
            chat_session_id = None

        if chat_session_id:
            chat_context = get_chat_context(chat_session_id)

        cs_id = create_chat_message(agent_instance, request.user, text_input, chat_session_id, 'user',
                                    request.user.first_name)
        exchange_rates = get_exchange_rates_for_user(agent_instance)
        system_content = generate_system_content(agent_data, chat_context, exchange_rates)

        user_messages = [
            {"role": "user", "content": text_input},
            {"role": "system", "content": system_content}
        ]

        response_message = handle_openai_response(agent_data, request.user, user_messages, agent_instance, cs_id,
                                                  request.user.first_name)

        if response_message is None:
            return Response({'error': 'Rate limit reached'}, status=status.HTTP_429_TOO_MANY_REQUESTS)

        create_chat_message(agent_instance, request.user, response_message, cs_id, 'agent', agent_instance.name)

        return Response({'response': response_message, 'chat_session_id': cs_id})
